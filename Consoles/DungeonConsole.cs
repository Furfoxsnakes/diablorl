using System;
using Microsoft.Xna.Framework;
using SadConsole.Entities;
using SharpDX.Direct3D9;

namespace DiabloRL.Consoles
{
    public class DungeonConsole : SadConsole.Console
    {
        public EntityManager Actors = new EntityManager();
        
        public DungeonConsole(int xPos, int yPos, int width, int height) : base(width, height)
        {
            Position = new Point(xPos, yPos);
            Children.Add(Actors);
        }

        public void UpdateViewport()
        {
            ViewPort = new Rectangle(Game.Player.Position.X - (Width / 2), Game.Player.Position.Y - ((Height - 24) / 2), Game.Width, Game.Height - 20);
        }

        public override void Draw(TimeSpan timeElapsed)
        {
            base.Draw(timeElapsed);
            ViewPort = new Rectangle(Game.Player.Position.X - (Width / 2), Game.Player.Position.Y - ((Height - 24) / 2), Game.Width, Game.Height - 10);
        }
    }
}
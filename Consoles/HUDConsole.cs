using DiabloRL.Themes;
using Microsoft.Xna.Framework;
using SadConsole;
using SadConsole.Controls;
using SadConsole.Surfaces;
using SadConsole.Themes;

namespace DiabloRL.Consoles
{
    public class HUDConsole : SadConsole.ControlsConsole
    {
        public MessageLogConsole MessageLog;
        
        public HUDConsole() : base(Game.Width, 12)
        {
            Position = new Point(0, Game.Height - Height);
            Theme = new HUDConsoleTheme();

            var borderSurface = new Basic(Width, Height, base.Font);
            borderSurface.DrawBox(new Rectangle(0, 0, borderSurface.Width, borderSurface.Height), new Cell(Color.White, Color.Black), null, SadConsole.Surfaces.SurfaceBase.ConnectedLineThin);
            //borderSurface.Position = new Point(-1, -1);
            Children.Add(borderSurface);
            
            /* BUTTONS */
            var charButton = MakeButton(10, 1, 1, 1, "CHAR");
            var questButton = MakeButton(10, 1, 1, 4, "QUESTS");
            var mapButton = MakeButton(10, 1, 1, 7, "MAP");
            var menuButton = MakeButton(10, 1, 1, 10, "MENU");
            var invetoryBytton = MakeButton(10, 1, Width - 11, 1, "INV");
            var spellsButton = MakeButton(10, 1, Width - 11, 4, "SPELLS");
            var activeSpellButton = MakeButton(10, 1, Width - 11, 4, "FIREBALL");
            
            /* HP & MP LABELS */
            var hpTextbox = MakeLabel(15, 1, 15, 1, "HP: 999/999");
            var mpTextbox = MakeLabel(15, 1, Width - 26, 1, "MP: 999/999");
            
            /* BARS */
            var hpBar = MakeBar(2, 10, 12, 1, Color.Red, Color.DarkRed);
            hpBar.Progress = 0.5f;

            var mpBar = MakeBar(2, 10, Width - 14, 1, Color.Blue, Color.DarkBlue);
            mpBar.Progress = 0.5f;

            MessageLog = new MessageLogConsole();
            Children.Add(MessageLog);
        }

        private Button MakeButton(int width, int height, int x, int y, string text)
        {
            var button = new Button(width, height)
            {
                Position = new Point(x, y), 
                Text = text,
                Theme = new HUDButton()
            };
            
            Add(button);
            return button;
        }

        private TextBox MakeTextBox(int width, int x, int y, string text)
        {
            var textBox = new TextBox(width)
            {
                Position = new Point(x, y),
                Text = text
            };

            Add(textBox);
            return textBox;
        }

        private Basic MakeLabel(int width, int height, int x, int y, string text)
        {
            var surface = new Basic(width, height)
            {
                Position = new Point(x, y)
            };
            surface.Print(0, 0, text);

            Children.Add(surface);
            return surface;
        }

        private ProgressBar MakeBar(int width, int height, int x, int y, Color fg, Color bg)
        {
            var theme = new ProgressBarTheme();
            theme.Foreground.Normal = new Cell(Color.White, fg);
            theme.Background.Normal = new Cell(Color.White, bg);
            
            var bar = new ProgressBar(width, height, VerticalAlignment.Bottom)
            {
                Position = new Point(x, y),
                Theme =  theme
            };

            Add(bar);
            return bar;
        }
    }
}
using Microsoft.Xna.Framework;
using SadConsole.Surfaces;
using SadConsole;

namespace DiabloRL.Consoles
{
    public class MessageLogConsole : SadConsole.Console
    {
        public MessageLogConsole() : base(48, 7)
        {
            Position = new Point(16, 3);
            Print(0, 0, "Message Log");
            Print(0, 1, "Message Log");
            Print(0, 2, "Message Log");
            Print(0, 3, "Message Log");
            Print(0, 4, "Message Log");
            Print(0, 5, "Message Log");
            Print(0, 6, "Message Log");
            
            var borderSurface = new Basic(Width + 2, Height + 2, base.Font);
            borderSurface.DrawBox(new Rectangle(0, 0, borderSurface.Width, borderSurface.Height), new Cell(Color.White, Color.Black), null, SadConsole.Surfaces.SurfaceBase.ConnectedLineThin);
            borderSurface.Position = new Point(-1, -1);
            Children.Add(borderSurface);
        }
    }
}
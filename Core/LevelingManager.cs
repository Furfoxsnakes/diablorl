using System;
using DiabloRL.Entities.Components;

namespace DiabloRL.Core
{
    public abstract class LevelingManager
    {
        public int Exp { get; set; }
        public EntityStats ComparableStats { get; set; }

        public LevelingManager(EntityStats stats)
        {
            ComparableStats = stats;
            Exp = 0;
        }

        public int ExpForNextLevel()
        {
            return (int)Math.Floor(Math.Log(ComparableStats.Level + 1) * 100);
        }

        public void AddXp(int amount)
        {
            var xpThreshold = ExpForNextLevel();

            if (Exp + amount >= xpThreshold)
            {
                var remainder = amount - xpThreshold;
                LevelUp(remainder);

                if (remainder > 0)
                    AddXp(remainder);
            }
            else
            {
                Exp += amount;
            }
        }

        protected virtual void LevelUp(int remainder)
        {
            ComparableStats.Level++;
            Exp = 0;
        }
    }
}
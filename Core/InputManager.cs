using DiabloRL.Entities;
using DiabloRL.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SharpDX.DXGI;

namespace DiabloRL.Core
{
    public static class InputManager
    {
        public static bool HandleKeys(GameStates state)
        {
            var left = SadConsole.Global.KeyboardState.IsKeyPressed(Keys.Left);
            var right = SadConsole.Global.KeyboardState.IsKeyPressed(Keys.Right);
            var up = SadConsole.Global.KeyboardState.IsKeyPressed(Keys.Up);
            var down = SadConsole.Global.KeyboardState.IsKeyPressed(Keys.Down);
            var escape = SadConsole.Global.KeyboardState.IsKeyPressed(Keys.Escape);

            var movement = new Point();

            switch (state)
            {
                case GameStates.PlayerTurn:
                {
                    
                    if (left)
                    {
                        movement = new Point(-1, 0);
                    }
                    else if (right)
                    {
                        movement = new Point(1, 0);
                    }

                    if (up)
                    {
                        movement = new Point(0, -1);
                    }
                    else if (down)
                    {
                        movement = new Point(0, 1);
                    }

                    break;
                }
            }

            if (Game.Player.MoveBy(movement, Game.AdventureScreen.Map)) return true;
            
            // player couldn't move
            // get the monster at the requested position
            var newPos = Game.Player.Position + movement;
            var actor = Game.AdventureScreen.Map.GetActorAtPos(newPos.X, newPos.Y);
            
            // don't do anything yet if the actor is null or is not a monster
            if (actor == null || !(actor is Monster)) return false;

            Attack(Game.Player, actor);
            return true;
        }

        private static void Attack(Actor attacker, Actor defender)
        {
            defender.TakeDamage(1);
        }
    }
}
using System;
using DiabloRL.Cartography;
using DiabloRL.Entities;
using DiabloRL.Enums;
using DiabloRL.Screens;
using SadConsole;
using Console = SadConsole.Console;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueSharp.Random;

namespace DiabloRL
{
    class Game
    {

        public const int Width = 80;
        public const int Height = 60;
        public static AdventureScreen AdventureScreen { get; set; }
        public static Player Player { get; set; }
        public static GameStates GameState { get; set; }
        public static DotNetRandom Random { get; set; }

        private static void Main(string[] args)
        {
            // Setup the engine and creat the main window.
            SadConsole.Game.Create("Cheepicus12.font", Width, Height);

            // Hook the start event so we can add consoles to the system.
            SadConsole.Game.OnInitialize = Init;

            // Hook the update event that happens each frame so we can trap keys and respond.
            SadConsole.Game.OnUpdate = Update;
                        
            // Start the game.
            SadConsole.Game.Instance.Run();

            //
            // Code here will not run until the game window closes.
            //
            
            SadConsole.Game.Instance.Dispose();
        }
        
        private static void Update(GameTime time)
        {
            // Called each logic update.
        }

        private static void Init()
        {
            // Any custom loading and prep. We will use a sample console for now
            var seed = (int)DateTime.UtcNow.Ticks;
            Random = new DotNetRandom(seed);

            Player = new Player
            {
                Position = new Point(2, 2)
            };
            AdventureScreen = new AdventureScreen();
            var dunGen = new DungeonGenerator(100, 100, 7, 12, 40);
            AdventureScreen.Map = dunGen.CreateMap();
            AdventureScreen.Map.AddActor(Player);
            Player.Position = new Point(AdventureScreen.Map.Rooms[0].Center.X, AdventureScreen.Map.Rooms[0].Center.Y);
            GameState = GameStates.PlayerTurn;

            // Set our new console as the thing to render and process
            Global.CurrentScreen.Children.Add(AdventureScreen);
        }
    }
}

namespace DiabloRL.Exceptions
{
    public class BaseException
    {
        public bool Toggle { get; private set; }
        private bool _defaultToggle;

        public BaseException(bool defaultToggle)
        {
            _defaultToggle = defaultToggle;
            Toggle = defaultToggle;
        }

        public void FlipToggle()
        {
            Toggle = !_defaultToggle;
        }
    }
}
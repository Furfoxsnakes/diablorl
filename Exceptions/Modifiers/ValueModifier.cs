namespace DiabloRL.Exceptions.Modifiers
{
    public abstract class ValueModifier : Modifier
    {
        protected ValueModifier(int sortOrder) : base(sortOrder)
        {
            // Nothing
        }

        public abstract float Modify(float value);
    }
}
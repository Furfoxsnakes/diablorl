namespace DiabloRL.Exceptions.Modifiers
{
    public abstract class Modifier
    {
        public readonly int SortOrder;

        public Modifier(int sortOrder)
        {
            sortOrder = sortOrder;
        }
    }
}
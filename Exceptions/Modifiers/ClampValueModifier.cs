using System;

namespace DiabloRL.Exceptions.Modifiers
{
    public class ClampValueModifier : ValueModifier
    {
        public readonly float Min;
        public readonly float Max;
        
        public ClampValueModifier(int sortOrder, float min, float max) : base(sortOrder)
        {
            Min = min;
            Max = max;
        }

        public override float Modify(float value)
        {
            //TODO: Implement clamping using .NET 2.0 math
            return value;
        }
    }
}
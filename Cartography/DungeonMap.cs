using System.Collections.Generic;
using DiabloRL.Consoles;
using DiabloRL.Entities;
using Microsoft.Xna.Framework;
using RogueSharp;
using Point = Microsoft.Xna.Framework.Point;
using Rectangle = RogueSharp.Rectangle;

namespace DiabloRL.Cartography
{
    public class DungeonMap : Map
    {
        public List<Rectangle> Rooms { get; set; } = new List<Rectangle>();
        public List<Actor> Actors { get; private set; } = new List<Actor>();

        public DungeonMap()
        {
            
        }

        public void Draw(DungeonConsole console)
        {
            foreach (var cell in GetAllCells())
            {
                SetCellGlyph(cell, console);
            }
        }

        public void AddActor(Actor actor)
        {
            Game.AdventureScreen.DungeonConsole.Actors.Entities.Add(actor);
            //Game.AdventureScreen.DungeonConsole.Children.Add(actor);
            SetCellIsWalkable(actor.Position, false);
        }

        public void RemoveActor(Actor actor)
        {
//            if (!Actors.Contains(actor))
//                return;

            if (!Game.AdventureScreen.DungeonConsole.Actors.Entities.Contains(actor))
                return;

            Game.AdventureScreen.DungeonConsole.Actors.Entities.Remove(actor);
            SetCellIsWalkable(actor.Position, true);
        }

        public void ComputePlayerFov()
        {
            var fovCells = ComputeFov(Game.Player.Position.X, Game.Player.Position.Y, 10, true);

            foreach (var cell in fovCells)
            {
                SetCellProperties(cell.X, cell.Y, cell.IsTransparent, cell.IsWalkable, true);
            }
        }

        public void SetCellIsWalkable(int x, int y, bool isWalkable)
        {
            var cell = GetCell(x, y);
            SetCellProperties(x, y, cell.IsTransparent, isWalkable, cell.IsExplored);
        }

        public void SetCellIsWalkable(Point p, bool isWalkable)
        {
            SetCellIsWalkable(p.X, p.Y, isWalkable);
        }

        public Actor GetActorAtPos(int x, int y)
        {
            foreach (var entity in Game.AdventureScreen.DungeonConsole.Actors.Entities)
            {
                if (entity.Position.X == x && entity.Position.Y == y)
                    return entity as Actor;
            }

            return null;
        }

        private void SetCellGlyph(ICell cell, DungeonConsole console)
        {
            if (!cell.IsExplored)
                return;

            if (cell.IsInFov)
            {
                if (!cell.IsTransparent && !cell.IsWalkable)
                {
                    // wall in FOV
                    console.SetGlyph(cell.X, cell.Y, '#', Color.White);
                }
                else
                {
                    // floor in FOV
                    console.SetGlyph(cell.X, cell.Y, '.', Color.White);
                }
            }
            else
            {
                if (!cell.IsTransparent && !cell.IsWalkable)
                {
                    // wall not in FOV
                    console.SetGlyph(cell.X, cell.Y, '#', Color.SlateGray);
                }
                else
                {
                    // floor in FOV
                    console.SetGlyph(cell.X, cell.Y, '.', Color.SlateGray);
                }
            }
        }
        
    }
}
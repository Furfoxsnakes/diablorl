using System;
using System.Linq;
using DiabloRL.Entities;
using DiabloRL.Enums;
using Microsoft.Xna.Framework;
using RogueSharp.MapCreation;
using Rectangle = RogueSharp.Rectangle;

namespace DiabloRL.Cartography
{
    public class DungeonGenerator
    {
        private readonly int _width;
        private readonly int _height;
        private readonly int _minRoomSize;
        private readonly int _maxRoomSize;
        private readonly int _maxNumRooms;

        private DungeonMap _map;

        public DungeonGenerator(int width, int height, int minRoomSize, int maxRoomSize, int maxNumRooms)
        {
            _width = width;
            _height = height;
            _minRoomSize = minRoomSize;
            _maxRoomSize = maxRoomSize;
            _maxNumRooms = maxNumRooms;
            
            _map = new DungeonMap();
        }

        public DungeonMap CreateMap()
        {
            _map.Initialize(_width, _height);

            for (var i = 0; i < _maxNumRooms; ++i)
            {
                var newRoomWidth = Game.Random.Next(_minRoomSize, _maxRoomSize);
                var newRoomHeight = Game.Random.Next(_minRoomSize, _maxRoomSize);
                var newRoomPosX = Game.Random.Next(1, _width - newRoomWidth - 1);
                var newRoomPosY = Game.Random.Next(1, _height - newRoomHeight - 1);
                
                var newRoom = new Rectangle(newRoomPosX, newRoomPosY, newRoomWidth, newRoomHeight);

                var doesIntersect = _map.Rooms.Any(room => room.Intersects(newRoom));

                if (doesIntersect) continue;
                
                _map.Rooms.Add(newRoom);
                CarveRoom(newRoom);
            }

            for (var r = 1; r < _map.Rooms.Count; ++r)
            {
                var prevRoom = _map.Rooms[r - 1];
                var currRom = _map.Rooms[r];

                if (Game.Random.Next(0, 1) == 1)
                {
                    CarveHorizontalTunnel(prevRoom.Center.X, currRom.Center.X, prevRoom.Center.Y);
                    CarveVerticalTunnel(prevRoom.Center.Y, currRom.Center.Y, currRom.Center.X);
                }
                else
                {
                    CarveVerticalTunnel(prevRoom.Center.Y, currRom.Center.Y, prevRoom.Center.X);
                    CarveHorizontalTunnel(prevRoom.Center.X, currRom.Center.X, currRom.Center.Y);
                }
            }

            foreach (var room in _map.Rooms)
            {
                if (Game.Random.Next(0, 100) <= 60)
                {
                    var numMonsters = Game.Random.Next(0, 4);

                    for (var m = 0; m < numMonsters; ++m)
                    {
                        var monsterPos = new Point(Game.Random.Next(room.Left, room.Right),
                            Game.Random.Next(room.Top, room.Bottom));
                        var monster = Skellyman.Create(MonsterDifficulties.Normal);
                        monster.Position = monsterPos;
                        _map.AddActor(monster);
                    }
                }
            }

            return _map;
        }

        private void CarveRoom(Rectangle room)
        {
            for (var x = room.Left; x <= room.Right; ++x)
            {
                for (var y = room.Top; y < room.Bottom; ++y)
                {
                    _map.SetCellProperties(x, y, true, true);
                }
            }
        }

        private void CarveHorizontalTunnel(int x1, int x2, int y)
        {
            for (var x = Math.Min(x1, x2); x <= Math.Max(x1, x2); ++x)
            {
                _map.SetCellProperties(x, y, true, true);
            }
        }

        private void CarveVerticalTunnel(int y1, int y2, int x)
        {
            for (var y = Math.Min(y1, y2); y < Math.Max(y1, y2); ++y)
            {
                _map.SetCellProperties(x, y, true, true);
            }
        }
    }
}
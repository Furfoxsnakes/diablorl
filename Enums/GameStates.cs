namespace DiabloRL.Enums
{
    public enum GameStates
    {
        Debug = 0,
        PlayerTurn = 1,
        EnemyTurn = 2,
        Paused = 3
    }
}
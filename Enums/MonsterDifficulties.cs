namespace DiabloRL.Enums
{
    public enum MonsterDifficulties
    {
        Normal = 0,
        Nightmare = 1,
        Hell = 2
    }
}
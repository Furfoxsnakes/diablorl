using Microsoft.Xna.Framework;
using SadConsole;

namespace DiabloRL.Screens
{
    public class DungeonScreen : ScreenObject
    {
        public int Width { get; private set; }
        public int Height { get; private set; }

        public DungeonScreen(int xPos, int yPos, int width, int height)
        {
            Position = new Point(xPos, yPos);
            Width = width;
            Height = height;
        }
    }
}
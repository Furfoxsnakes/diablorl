using System;
using DiabloRL.Cartography;
using DiabloRL.Consoles;
using DiabloRL.Core;
using DiabloRL.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SadConsole;

namespace DiabloRL.Screens
{
    public class AdventureScreen : ScreenObject
    {
        public DungeonConsole DungeonConsole { get; set; }
        public HUDConsole HudConsole { get; set; }
        public DungeonMap Map { get; set; }

        private bool drawRequired = true;

        public AdventureScreen()
        {
            DungeonConsole = new DungeonConsole(0, 0, 100, 100);
            Children.Add(DungeonConsole);
            HudConsole = new HUDConsole();
            Children.Add(HudConsole);
        }

        public override void Update(TimeSpan timeElapsed)
        {
            base.Update(timeElapsed);
            
            if (drawRequired)
            {
                Map.ComputePlayerFov();
                Map.Draw(DungeonConsole);
                //DungeonConsole.UpdateViewport();
                drawRequired = false;
            }

            drawRequired = InputManager.HandleKeys(Game.GameState);

            // quit game
            if (Global.KeyboardState.IsKeyPressed(Keys.Escape))
            {
                SadConsole.Game.Instance.Exit();
            }
        }
    }
}
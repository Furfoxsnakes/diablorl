using Microsoft.Xna.Framework;
using SadConsole;
using SadConsole.Themes;

namespace DiabloRL.Themes
{
    public class HUDButton : ButtonTheme
    {
        public HUDButton()
        {
            Normal = new Cell(Color.White, Color.Black);
            Selected = new Cell(Color.White, Colors.RedDark);
            Focused = new Cell(Color.White, Colors.RedDark);
            MouseOver = new Cell(Color.Red, Colors.Black);
            MouseDown = new Cell(Color.DarkRed, Colors.Black);
        }
    }
}
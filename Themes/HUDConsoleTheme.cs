using SadConsole;
using SadConsole.Themes;

namespace DiabloRL.Themes
{
    public class HUDConsoleTheme : ControlsConsoleTheme
    {
        public HUDConsoleTheme()
        {
            FillStyle = new Cell(Colors.White, Colors.Black);
            
        }
    }
}
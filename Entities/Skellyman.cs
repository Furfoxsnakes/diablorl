using DiabloRL.Enums;
using Microsoft.Xna.Framework;
using RogueSharp.DiceNotation;

namespace DiabloRL.Entities
{
    public class Skellyman : Monster
    {
        public static Skellyman Create(MonsterDifficulties difficulty)
        {
            var rand = Game.Random;

            var retValue = new Skellyman
            {
                Awareness = 10,
                FgColour = Color.AntiqueWhite,
                Glyph = 's',
                Name = "Skellyman",
                Speed = 14
            };
            
            switch (difficulty)
            {
                case MonsterDifficulties.Normal:
                {
                    retValue.Level = 1;
                    retValue.Health = rand.Next(1, 2);
                    retValue.MaxHealth = retValue.Health;
                    retValue.Attack = rand.Next(1, 4);
                    retValue.ArmourClass = 0;
                    retValue.ToHit = 20;
                    retValue.EXP = 64;
                    retValue.FireResist = 0;
                    retValue.LightResist = 0;
                    retValue.MagicResist = 100;
                    
                    break;
                }
                case MonsterDifficulties.Nightmare:
                {
                    retValue.Level = 1;
                    retValue.Health = rand.Next(1, 2);
                    retValue.MaxHealth = retValue.Health;
                    retValue.Attack = rand.Next(1, 4);
                    retValue.ArmourClass = 0;
                    retValue.ToHit = 20;
                    retValue.EXP = 64;
                    retValue.FireResist = 0;
                    retValue.LightResist = 0;
                    retValue.MagicResist = 100;
                    
                    break;
                }
                case MonsterDifficulties.Hell:
                {
                    retValue.Level = 1;
                    retValue.Health = rand.Next(1, 2);
                    retValue.MaxHealth = retValue.Health;
                    retValue.Attack = rand.Next(1, 4);
                    retValue.ArmourClass = 0;
                    retValue.ToHit = 20;
                    retValue.EXP = 64;
                    retValue.FireResist = 0;
                    retValue.LightResist = 0;
                    retValue.MagicResist = 100;
                    
                    break;
                }
            }
            
            //var health = Dice.Roll("2D5");
            return retValue;
        }
    }
}
using System;

namespace DiabloRL.Entities.Components
{
    public class EntityStats : IComparable
    {
        public int Level { get; set; }
        public int Exp { get; set; }
        public int Strength { get; set; }
        public int Magic { get; set; }
        public int Dexterity { get; set; }
        public int Vitality { get; set; }
        public int Life { get; set; }    // Max HP
        public int HP { get; set; }        // Current HP
        public int Mana { get; set; }    // Max MP
        public int MP { get; set; }        // Current MP

        public EntityStats()
        {
            Level = 1;
            Exp = 0;
        }
        
        public int CompareTo(object obj)
        {
            if (!(obj is EntityStats)) return 0;

            var stats = obj as EntityStats;
            
            if (stats.Level >= Level
                && stats.Strength >= Strength
                && stats.Magic >= Magic
                && stats.Dexterity >= Dexterity
                && stats.Vitality >= Vitality)
            {
                return 0;
            }

            return 1;
        }

        protected void Init()
        {
            HP = Life;
            MP = Mana;
        }
    }
}
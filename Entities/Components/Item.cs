namespace DiabloRL.Entities.Components
{
    public abstract class Item
    {
        public EntityStats RequiredStats { get; set; }
        public string Name { get; set; }

        public Item(EntityStats requiredStats, string name)
        {
            RequiredStats = requiredStats;
            Name = name;
        }

        public bool CanBeUsedByActor(Actor actor)
        {
            //TODO: implement this if need be
            return false;
        }
    }
}
namespace DiabloRL.Entities.Components
{
    public class Sword : Item
    {
        public Sword() : base(new EntityStats(), "Sword")
        {
            RequiredStats.Strength = 10;
        }
    }
}
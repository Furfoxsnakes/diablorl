using DiabloRL.Entities.Components;

namespace DiabloRL.Entities
{
    public class WarriorStats : CharacterStats
    {
        public WarriorStats()
        {
            Strength = 30;
            Magic = 10;
            Dexterity = 20;
            Vitality = 25;

            Init();
        }

        public override int Life => 2 * Vitality + 2 * Level + 18;
        public override int Mana => 1 * Magic + 1 * Level - 1;
    }
}
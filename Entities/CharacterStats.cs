using DiabloRL.Entities.Components;

namespace DiabloRL.Entities
{
    public abstract class CharacterStats : EntityStats
    {
        public abstract int Life { get; }
        public abstract int Mana { get; }
    }
}
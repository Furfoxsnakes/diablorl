using System;
using Microsoft.Xna.Framework;
using SadConsole.Themes;

namespace DiabloRL.Entities
{
    public class Monster : Actor
    {
        public override void Draw(TimeSpan timeElapsed)
        {
            // do not draw if the monster is not in FOV
            if (!Game.AdventureScreen.Map.IsInFov(Position.X, Position.Y)) return;
            
            base.Draw(timeElapsed);
        }
    }
}
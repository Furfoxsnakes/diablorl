using DiabloRL.Core;
using DiabloRL.Entities.Components;

namespace DiabloRL.Entities
{
    public class WarriorLeveling : LevelingManager
    {
        public WarriorLeveling(EntityStats stats) : base(stats)
        {
        }

        protected override void LevelUp(int remainder)
        {
            base.LevelUp(remainder);

            ComparableStats.Life += 2;
            ComparableStats.Mana += 1;
        }
    }
}
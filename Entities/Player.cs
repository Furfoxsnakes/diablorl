using DiabloRL.Entities.Components;
using Microsoft.Xna.Framework;
using SadConsole.Themes;

namespace DiabloRL.Entities
{
    public class Player : Actor
    {
        public Player()
        {
            Attack = 2;
            Awareness = 7;
            FgColour = Color.White;
            Defense = 2;
            Dodge = 50;
            EXP = 0;
            Glyph = '@';
            Gold = 0;
            Health = 100;
            MaxHealth = 100;
            Name = "Furfoxsnakes";
            Speed = 10;
            
            Stats = new WarriorStats();
            LevelingManager = new WarriorLeveling(Stats);
        }
    }
}
using System;
using DiabloRL.Cartography;
using DiabloRL.Core;
using DiabloRL.Entities.Components;
using Microsoft.Xna.Framework;
using RogueSharp;
using SadConsole;
using SadConsole.Entities;
using SadConsole.Surfaces;
using Point = Microsoft.Xna.Framework.Point;

namespace DiabloRL.Entities
{
    public class Actor : Entity
    {
        public Color FgColour
        {
            get => Animation.CurrentFrame[0].Foreground;
            set => Animation.CurrentFrame[0].Foreground = value;
        }

        public Color BgColour
        {
            get => Animation.CurrentFrame[0].Background;
            set => Animation.CurrentFrame[0].Background = value;
        }
        
        public int Glyph
        {
            get => Animation.CurrentFrame[0].Glyph;
            set => Animation.CurrentFrame[0].Glyph = value;
        }
        
        public int Attack { get; set; }
        public int ArmourClass { get; set; }
        public int Awareness { get; set; }
        public int Defense { get; set; }
        public int Dodge { get; set; }
        public int EXP { get; set; }
        public int Gold { get; set; }
        public int Health { get; set; }
        public int Level { get; set; }
        public int ToHit { get; set; }
        public int MaxHealth { get; set; }
        public string Name { get; set; }
        public int Speed { get; set; }
        
        public LevelingManager LevelingManager { get; set; }
        public EntityStats Stats { get; set; }
        
        // resists
        public int FireResist { get; set; }
        public int LightResist { get; set; }
        public int MagicResist { get; set; }
        
        protected Actor() : base(1,1)
        {
            Animation.CurrentFrame[0].Foreground = Color.White;
            Animation.CurrentFrame[0].Background = Color.Black;
            Animation.CurrentFrame[0].Glyph = '?';
        }

        public bool MoveBy(Point p, DungeonMap map)
        {
            if (!map.IsWalkable(Position.X + p.X, Position.Y + p.Y))
                return false;
            
            map.SetCellIsWalkable(Position, true);
            Position += p;
            map.SetCellIsWalkable(Position, false);
            
            return true;
        }

        public void TakeDamage(int amount)
        {
            Health -= amount;

            if (Health <= 0)
                Kill();
        }

        protected virtual void Kill()
        {
            System.Console.WriteLine($"{Name} has been slain!");
            Game.AdventureScreen.Map.RemoveActor(this);
        }
    }
    
}